class AddPostToGames < ActiveRecord::Migration
  def change
    add_reference :posts, :game, index: true, foreign_key: true
  end
end
