class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.string :name
      t.string :bio
      t.references :game, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_foreign_key :characters, :games
  end
end
