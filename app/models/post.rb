class Post < ActiveRecord::Base
	belongs_to :game
	validates :content, presence: true,
                    length: { minimum: 5 }
end
