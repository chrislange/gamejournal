class CharactersController < ApplicationController

def create
    @game = Game.find(params[:game_id])
    @character = @game.characters.create(character_params)
    if @character.save
       
       flash[:success] = "Saved!"
        redirect_to game_path(@game)      
      else
        flash[:alert] = "Could not save client"
        redirect_to @game
      end
  end

  def edit
    @game = Game.find(params[:game_id])
    @character = Character.find(params[:id])
  end

  def update
    @game = Game.find(params[:game_id])
    @character = Character.find(params[:id])
 
    if @character.update(character_params)
      redirect_to @game
    else
      render 'edit'
    end
  end

  def destroy
      @game = Game.find(params[:game_id])
      @character = @game.characters.find(params[:id])
      @character.destroy
      redirect_to game_path(@game)
    end
 
  private
    def character_params
      params.require(:character).permit(:name, :bio, :image)
    end
end
