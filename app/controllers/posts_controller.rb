class PostsController < ApplicationController
	before_action :set_post, only: [:show, :edit, :update, :destroy]

	def index
    @post = Post.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
  end

  # GET /games/new
  def new
    @post = Post.new
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
  	@game = Game.find(params[:game_id])
    @post = @game.posts.create(post_params)

   
      if @post.save
       
       flash[:success] = "Saved!"
        redirect_to game_path(@game)      
      else
        flash[:alert] = "Could not save client"
        redirect_to @game
      end
    end

    def destroy
      @game = Game.find(params[:game_id])
      @post = @game.posts.find(params[:id])
      @post.destroy
      redirect_to game_path(@game)
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:content)
    end
end
